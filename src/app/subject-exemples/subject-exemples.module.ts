import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page/page.component';
import { SubjectRoutingModule } from './subject.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    PageComponent,
  ],
  imports: [
    CommonModule,
    SubjectRoutingModule,
    SharedModule,
  ]
})
export class SubjectExemplesModule { }
