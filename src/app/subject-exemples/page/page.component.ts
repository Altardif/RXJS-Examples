import { Component, OnInit } from '@angular/core';
import { take, tap } from 'rxjs';
import { AsyncService } from '../services/async.service';
import { BehaviorService } from '../services/behavior.service';
import { NormalService } from '../services/normal.service';
import { ReplayService } from '../services/replay.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  constructor(
    public asyncService: AsyncService,
    public behaviorService: BehaviorService,
    public normalService: NormalService,
    public replaySubject: ReplayService) { }

  ngOnInit(): void {
  }

  public openReplaySub = () => {
    this.replaySubject.observable.pipe(take(3), tap(console.log)).subscribe();
  }

  public openNormalSub = () => {
    this.normalService.observable.pipe(take(1), tap(console.log)).subscribe();
  }

  public openBehaviorSub = () => {
    this.behaviorService.observable.pipe(take(1), tap(console.log)).subscribe();
  }

  public openAsyncSub = () => {
    this.asyncService.observable.pipe(take(1), tap(console.log)).subscribe();
  }

}
