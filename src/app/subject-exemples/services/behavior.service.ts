import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BehaviorService {
  private subjet = new BehaviorSubject<number>(0)
  private currVal = 0;

  public observable = this.subjet.asObservable();

  constructor() { }

  public increment = () => {
    this.subjet.next(++this.currVal);
  }

  public complete = () => {
    this.subjet.complete();
  }
}
