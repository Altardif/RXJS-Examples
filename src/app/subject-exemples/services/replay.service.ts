import { Injectable } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReplayService {

  private subjet = new ReplaySubject<number>(3)
  private currVal = 0;

  public observable = this.subjet.asObservable();

  constructor() {  }

  public increment = () => {
    this.subjet.next(++this.currVal);
  }

  public complete = () => {
    this.subjet.complete();
  }
}
