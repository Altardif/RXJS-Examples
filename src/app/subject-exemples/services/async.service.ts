import { Injectable } from '@angular/core';
import { AsyncSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsyncService {
  private subjet = new AsyncSubject<number>()
  private currVal = 0;

  public observable = this.subjet.asObservable();

  constructor() { }

  public increment = () => {
    this.subjet.next(++this.currVal);
  }

  public complete = () => {
    this.subjet.complete();
  }
}
