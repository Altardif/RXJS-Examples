import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NormalService {

  private subjet = new Subject<number>()
  private currVal = 0;

  public observable = this.subjet.asObservable();

  constructor() { }

  public increment = () => {
    this.subjet.next(++this.currVal);
  }

  public complete = () => {
    this.subjet.complete();
  }
}
