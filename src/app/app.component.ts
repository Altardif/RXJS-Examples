import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RXJSExempleService } from './rxjs-exemple.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  constructor(public service: RXJSExempleService) { }
  title = 'RXJS-Workshop';
}
