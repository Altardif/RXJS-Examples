import { Injectable } from '@angular/core';
import { AsyncSubject, BehaviorSubject, ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RXJSExempleService {
  private _behaviour = new BehaviorSubject(false);
  private _replay = new ReplaySubject();
  private _subject = new Subject();
  private _async = new AsyncSubject();

  public behaviour = this._behaviour.asObservable();
  public replay = this._replay.asObservable();
  public subject = this._subject.asObservable();
  public async = this._async.asObservable();

  public toFalse = () => {
    this._behaviour.next(false);
    this._replay.next(false);
    this._subject.next(false);
    this._async.next(false);
  }

  public toTrue = () => {
    this._behaviour.next(true);
    this._replay.next(true);
    this._subject.next(true);
    this._async.next(true);
  }

  public complete = () => {
    this._behaviour.complete();
    this._replay.complete();
    this._subject.complete();
    this._async.complete();
  }
}
