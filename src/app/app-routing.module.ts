import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from './subject-exemples/page/page.component';

const routes: Routes = [
  {
    path: 'subject',
    loadChildren: () => import('./subject-exemples/subject-exemples.module')
      .then(m => m.SubjectExemplesModule),
  },
  {
    path: 'mapping',
    loadChildren: () => import('./mapping-exemples/mapping-exemples.module')
      .then(m => m.MappingExemplesModule),
  },
  {
    path: 'async',
    loadChildren: () => import('./async-exemples/async-exemples.module')
      .then(m => m.AsyncExempleModule),
  },
  {
    path: 'operator',
    loadChildren: () => import('./operators-exemples/operator-exemples.module')
      .then(m => m.OperatorExempleModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
