import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsyncRoutingModule } from './async.routes';
import { SharedModule } from '../shared/shared.module';
import { OperatorPageComponent } from './operator-page/operator-page.component';

@NgModule({
  declarations: [
    OperatorPageComponent,
  ],
  imports: [
    CommonModule,
    AsyncRoutingModule,
    SharedModule,
  ]
})
export class OperatorExempleModule { }
