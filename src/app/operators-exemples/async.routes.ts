import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperatorPageComponent } from './operator-page/operator-page.component';

const routes: Routes = [
  {
    path: '',
    component: OperatorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsyncRoutingModule { }
