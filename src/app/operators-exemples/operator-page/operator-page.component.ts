import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { catchError, delay, first, fromEvent, interval, map, mergeMap, Observable, of, pluck, switchMap, tap } from 'rxjs';
import { BackendService } from 'src/app/mapping-exemples/services/backend.service';

@Component({
  selector: 'app-operator-page',
  templateUrl: './operator-page.component.html',
  styleUrls: ['./operator-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OperatorPageComponent implements AfterViewInit {
  @ViewChild('btn') public boutton: ElementRef | undefined

  public pluck$: Observable<unknown>;
  public first$: Observable<unknown>;
  public buttonEvent$: Observable<unknown> | undefined;


  constructor(public backend: BackendService) {
    this.pluck$ = backend.getBackend().pipe(pluck('access', 'name'))
    this.first$ = of(1, 2, 3, 4).pipe(
      delay(1000),
    )
  }

  ngAfterViewInit() {
    fromEvent(this.boutton?.nativeElement, 'click').pipe(map(() => 'test'), tap(console.log))
  }


}
