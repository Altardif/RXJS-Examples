import { Injectable } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';

const backendResponse = {
  access: {
    name: 'Test',
    modules: [
      {
        name: 'Module 1',
        hasAccess: false,
      },
      {
        name: 'Module 2',
        hasAccess: true,
      }
    ]
  }
};

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private _request = new ReplaySubject();


  constructor() {
    this._request.next(backendResponse)
  }

  public getBackend = () => {
    return this._request.asObservable();
  }
}
