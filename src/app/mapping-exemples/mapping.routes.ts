import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MappingPageComponent } from './mapping-page/mapping-page.component';

const routes: Routes = [
  {
    path: '',
    component: MappingPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MappingRoutingModule { }
