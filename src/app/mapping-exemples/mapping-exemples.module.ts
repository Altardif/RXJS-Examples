import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MappingPageComponent } from './mapping-page/mapping-page.component';
import { MappingRoutingModule } from './mapping.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    MappingPageComponent
  ],
  imports: [
    CommonModule,
    MappingRoutingModule,
    SharedModule
  ]
})
export class MappingExemplesModule { }
