import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { BackendService } from '../services/backend.service';

@Component({
  selector: 'app-mapping-page',
  templateUrl: './mapping-page.component.html',
  styleUrls: ['./mapping-page.component.scss']
})
export class MappingPageComponent implements OnInit {

  public mappedResponse: Observable<boolean[]>;
  public response: Observable<unknown>;

  constructor(public backend: BackendService) {
    this.response = this.backend.getBackend();
    this.mappedResponse = this.response.pipe(
      map((response: any) => response.access.modules.map((module: any) => module.hasAccess)),
    )
  }

  ngOnInit(): void {
  }

}
