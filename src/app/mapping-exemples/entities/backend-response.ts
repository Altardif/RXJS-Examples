export interface Response {
  access: Access
}

export interface Access {
  name: string,
  modules: Module[]
}

export interface Module {
  name: string,
  hasAccess: boolean,
}
