import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsyncPageComponent } from './async-page/async-page.component';

const routes: Routes = [
  {
    path: '',
    component: AsyncPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsyncRoutingModule { }
