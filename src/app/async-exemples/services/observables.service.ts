import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObservablesService {

  private subjet = new BehaviorSubject(0);
  private noAsync = new BehaviorSubject(0);
  private currAsyncVal = 0;
  private currNoAsyncVal = 0;

  public observable = this.subjet.asObservable();
  public noAsyncObservable = this.noAsync.asObservable();


  constructor() { }


  public incrementAsync = () => {
    this.subjet.next(++this.currAsyncVal);
  }

  public incrementNoAsync = () => {
    this.noAsync.next(++this.currNoAsyncVal);
  }
}
