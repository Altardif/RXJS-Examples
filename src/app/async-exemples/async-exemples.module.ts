import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsyncPageComponent } from './async-page/async-page.component';
import { AsyncRoutingModule } from './async.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AsyncPageComponent,
  ],
  imports: [
    CommonModule,
    AsyncRoutingModule,
    SharedModule,
  ]
})
export class AsyncExempleModule { }
