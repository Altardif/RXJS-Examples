import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ObservablesService } from '../services/observables.service';

@Component({
  selector: 'app-async-page',
  templateUrl: './async-page.component.html',
  styleUrls: ['./async-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AsyncPageComponent implements OnDestroy {

  public value: number | undefined;
  public sub: Subscription | undefined

  constructor(public observableService: ObservablesService) {
    const sub = observableService.noAsyncObservable.subscribe((val) => this.value = val);
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe()
  }

}
